package filters

import (
	"bytes"
	"encoding/binary"
	"encoding/gob"
	"hash"
	"hash/fnv"
	"io"
	"math"

	log "github.com/sirupsen/logrus"
	"mediamath.com/device-strategy-service/metrics"
)

// BloomFilter implements a classic Bloom filter. A Bloom filter has a non-zero
// probability of false positives and a zero probability of false negatives.
type BloomFilter struct {
	BucketArray *Buckets    // filter Data
	HashFunc    hash.Hash64 // HashFunc function (kernel for all KValue functions)
	M           uint        // filter size
	KValue      uint        // number of HashFunc functions
	CountValue  uint        // number of items added
}

// NewBloomFilter creates a new Bloom filter optimized to store n items with a
// specified target false-positive rate.
func NewBloomFilter(n uint, fpRate float64) *BloomFilter {
	m := OptimalM(n, fpRate)
	return &BloomFilter{
		BucketArray: NewBuckets(m, 1),
		HashFunc:    fnv.New64(),
		M:           m,
		KValue:      OptimalK(fpRate),
	}
}

// Capacity returns the Bloom filter capacity, M.
func (b *BloomFilter) Capacity() uint {
	return b.M
}

// K returns the number of HashFunc functions.
func (b *BloomFilter) K() uint {
	return b.KValue
}

// Count returns the number of items added to the filter.
func (b *BloomFilter) Count() uint {
	return b.CountValue
}

// EstimatedFillRatio returns the current estimated ratio of set bits.
func (b *BloomFilter) EstimatedFillRatio() float64 {
	return 1 - math.Exp((-float64(b.CountValue)*float64(b.KValue))/float64(b.M))
}

// FillRatio returns the ratio of set bits.
func (b *BloomFilter) FillRatio() float64 {
	sum := uint32(0)
	for i := uint(0); i < b.BucketArray.Count(); i++ {
		sum += b.BucketArray.Get(i)
	}
	return float64(sum) / float64(b.M)
}

// Test will test for membership of the Data and returns true if it is a
// member, false if not. This is a probabilistic test, meaning there is a
// non-zero probability of false positives but a zero probability of false
// negatives.
func (b *BloomFilter) Test(data []byte) bool {
	lower, upper := hashKernel(data, b.HashFunc)

	// If any of the K bits are not set, then it's not a member.
	for i := uint(0); i < b.KValue; i++ {
		if b.BucketArray.Get((uint(lower)+uint(upper)*i)%b.M) == 0 {
			return false
		}
	}

	return true
}

// Add will add the Data to the Bloom filter. It returns the filter to allow
// for chaining.
func (b *BloomFilter) Add(data []byte) *BloomFilter {
	lower, upper := hashKernel(data, b.HashFunc)

	// Set the K bits.
	for i := uint(0); i < b.KValue; i++ {
		b.BucketArray.Set((uint(lower)+uint(upper)*i)%b.M, 1)
	}

	b.CountValue++
	return b
}

// TestAndAdd is equivalent to calling Test followed by Add. It returns true if
// the Data is a member, false if not.
func (b *BloomFilter) TestAndAdd(data []byte) bool {
	lower, upper := hashKernel(data, b.HashFunc)
	member := true

	// If any of the K bits are not set, then it's not a member.
	for i := uint(0); i < b.KValue; i++ {
		idx := (uint(lower) + uint(upper)*i) % b.M
		if b.BucketArray.Get(idx) == 0 {
			member = false
		}
		b.BucketArray.Set(idx, 1)
	}

	b.CountValue++
	return member
}

// Reset restores the Bloom filter to its original state. It returns the filter
// to allow for chaining.
func (b *BloomFilter) Reset() *BloomFilter {
	b.BucketArray.Reset()
	return b
}

// SetHash sets the hashing function used in the filter.
// For the effect on false positive rates see: https://github.com/tylertreat/BoomFilters/pull/1
func (b *BloomFilter) SetHash(h hash.Hash64) {
	b.HashFunc = h
}

// WriteTo writes a binary representation of the BloomFilter to an i/o stream.
// It returns the number of bytes written.
func (b *BloomFilter) WriteTo(stream io.Writer) (int64, error) {
	err := binary.Write(stream, binary.BigEndian, uint64(b.CountValue))
	if err != nil {
		return 0, err
	}
	err = binary.Write(stream, binary.BigEndian, uint64(b.M))
	if err != nil {
		return 0, err
	}
	err = binary.Write(stream, binary.BigEndian, uint64(b.KValue))
	if err != nil {
		return 0, err
	}

	writtenSize, err := b.BucketArray.WriteTo(stream)
	if err != nil {
		return 0, err
	}

	return writtenSize + int64(3*binary.Size(uint64(0))), err
}

// ReadFrom reads a binary representation of BloomFilter (such as might
// have been written by WriteTo()) from an i/o stream. It returns the number
// of bytes read.
func (b *BloomFilter) ReadFrom(stream io.Reader) (int64, error) {
	var count, m, k uint64
	var buckets Buckets

	err := binary.Read(stream, binary.BigEndian, &count)
	if err != nil {
		return 0, err
	}
	err = binary.Read(stream, binary.BigEndian, &m)
	if err != nil {
		return 0, err
	}
	err = binary.Read(stream, binary.BigEndian, &k)
	if err != nil {
		return 0, err
	}

	readSize, err := buckets.ReadFrom(stream)
	if err != nil {
		return 0, err
	}

	b.CountValue = uint(count)
	b.M = uint(m)
	b.KValue = uint(k)
	b.BucketArray = &buckets
	return readSize + int64(3*binary.Size(uint64(0))), nil
}

// GobEncode implements gob.GobEncoder interface.
func (b *BloomFilter) GobEncode() ([]byte, error) {
	var buf bytes.Buffer
	_, err := b.WriteTo(&buf)
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

// GobDecode implements gob.GobDecoder interface.
func (b *BloomFilter) GobDecode(data []byte) error {
	buf := bytes.NewBuffer(data)
	_, err := b.ReadFrom(buf)

	return err
}

func SerializeBloomFilter(filter *BloomFilter) (bytes.Buffer, error) {
	var byteBuffer bytes.Buffer
	enc := gob.NewEncoder(&byteBuffer)
	gob.Register(filter.HashFunc)
	// Encode (send) the value.
	err := enc.Encode(filter)
	if err != nil {
		metrics.GetCounter("BloomFilter", "error_encoding_filter").Inc()
		log.Error("encode error:", err)
		return byteBuffer, err
	}
	return byteBuffer, nil
}

func numberToBytes(x int64) []byte {
	buf := make([]byte, binary.MaxVarintLen64)
	binary.PutVarint(buf, x)
	return buf
}

func DeserializeBloomBytes(byteBuffer bytes.Buffer) (*BloomFilter, error) {

	dec := gob.NewDecoder(&byteBuffer)
	filterLoaded := &BloomFilter{} // sometimes fails
	//var filterLoaded *BloomFilter // always fails
	err := dec.Decode(filterLoaded)
	if err != nil {
		metrics.GetCounter("BloomFilter", "error_decoding_filter").Inc()
		//log.Error("decode error:", err)
		return nil, err
	}
	if filterLoaded.HashFunc == nil {
		log.Error("filter got with nil hash func. classic.go")
		filterLoaded.HashFunc = fnv.New64()
	}

	return filterLoaded, nil
}
