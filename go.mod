module mediamath.com/device-strategy-service

go 1.14

require (
	github.com/aerospike/aerospike-client-go v2.7.2+incompatible
	github.com/cornelk/hashmap v1.0.1
	github.com/d4l3k/messagediff v1.2.1
	github.com/dgraph-io/ristretto v0.0.1
	github.com/golang/protobuf v1.3.3
	github.com/google/go-cmp v0.4.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/labstack/gommon v0.3.0
	github.com/onsi/ginkgo v1.7.0 // indirect
	github.com/onsi/gomega v1.4.3 // indirect
	github.com/prometheus/client_golang v1.0.0
	github.com/prometheus/common v0.9.1 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/stretchr/testify v1.4.0
	github.com/tylertreat/BoomFilters v0.0.0-20181028192813-611b3dbe80e8
	github.com/yuin/gopher-lua v0.0.0-20191220021717-ab39c6098bdb // indirect
	golang.org/x/sync v0.0.0-20190911185100-cd5d95a43a6e // indirect
	golang.org/x/sys v0.0.0-20200122134326-e047566fdf82 // indirect
	google.golang.org/grpc v1.27.0
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.2.5 // indirect
)
