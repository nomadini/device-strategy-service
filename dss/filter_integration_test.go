package dss

import (
	"bytes"
	"testing"
	"time"

	"github.com/labstack/gommon/random"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"mediamath.com/device-strategy-service/filters"
	qual "mediamath.com/device-strategy-service/pb"
)

type FilterTestSuite struct {
	suite.Suite
	connector     *AerospikeConnector
	random        *random.Random
	namespaceName string
	setName       string
}

func (suite *FilterTestSuite) SetupSuite() () {

	logrus.SetLevel(logrus.DebugLevel)

	suite.random = random.New()
	suite.namespaceName = "test"
	suite.setName = "dss_test"
	suite.connector = NewAerospikeConnector(
		"localhost", 3000, suite.namespaceName, suite.setName, 60) // seconds
}

func (suite *FilterTestSuite) SetupTest() {
	err := suite.connector.Client.Truncate(nil, suite.namespaceName, suite.setName, nil)
	require.NoError(suite.T(), err)
}

func TestFilterTestSuite(t *testing.T) {
	suite.Run(t, new(FilterTestSuite))
}

func (suite *FilterTestSuite) TearDownTest() {

}

func (suite *FilterTestSuite) TestFilter() {
	//create the filter and deviceId
	numberOfSegments := 1000
	deviceId := GetRandomDeviceId()
	cfilter := filters.NewBloomFilter(uint(numberOfSegments), 0.0001)

	// add segments to the filter
	segments := thenSegmentsAreAddedToFilter(numberOfSegments, cfilter)

	// serialize and deserialize the filter
	thenFilterIsSerializedInToKV(cfilter, suite, deviceId)
	filterCreated := thenFilterIsReadFromKV(suite, deviceId)

	// test the filter
	thenExistingSegmentsAreFoundInFilter(segments, filterCreated, suite)
	thenNotExistingSegmentsAreNotFoundInFilter(numberOfSegments, filterCreated, suite)
}

func thenNotExistingSegmentsAreNotFoundInFilter(numberOfSegments int, filterCreated *filters.BloomFilter, suite *FilterTestSuite) {
	segmentsNotToInsert := createNotExistingSegments(numberOfSegments)
	for _, s := range segmentsNotToInsert {
		isFound := filterCreated.Test(SegmentToBytes(&s))
		logrus.WithField("isFound", isFound).WithField("segment", s).Debug("segment results : ")
		suite.False(isFound)
	}
}

func thenExistingSegmentsAreFoundInFilter(segments []qual.Segment, filterCreated *filters.BloomFilter, suite *FilterTestSuite) {
	for _, s := range segments {
		isFound := filterCreated.Test(SegmentToBytes(&s))
		suite.True(isFound)
	}
}

func thenFilterIsReadFromKV(suite *FilterTestSuite, deviceId string) []*filters.BloomFilter {
	filterBytes, err := suite.connector.getFilters(deviceId)
	suite.NoError(err)
	var filtersCreated []*filters.BloomFilter
	for _, bytess := range filterBytes {
		var byteBuffer bytes.Buffer
		byteBuffer.Write(bytess)
		filterCreated, err := filters.DeserializeBloomBytes(byteBuffer)
		suite.NoError(err)
		filtersCreated = append(filtersCreated, filterCreated)
	}

	return filtersCreated
}

func thenFilterIsSerializedInToKV(cfilter *filters.BloomFilter, suite *FilterTestSuite, deviceId string) {
	byteBufferIn, err := filters.SerializeBloomFilter(cfilter)
	suite.NoError(err)
	err = suite.connector.persistDeviceIdFilterBytes(deviceId, byteBufferIn.Bytes())
	suite.NoError(err)
}

func createNotExistingSegments(numberOfSegments int) []qual.Segment {
	var segmentsNotToInsert []qual.Segment
	endOfSegmentNum := numberOfSegments * 2
	for i := numberOfSegments; i < endOfSegmentNum; i++ {
		segmentsNotToInsert = append(segmentsNotToInsert, GetRandomSegmentWithId(int64(i)))
	}
	return segmentsNotToInsert
}

func thenSegmentsAreAddedToFilter(numberOfSegments int, cfilter *filters.BloomFilter) []qual.Segment {
	var segments []qual.Segment
	for i := 0; i < numberOfSegments; i++ {
		segments = append(segments, GetRandomSegmentWithId(int64(i)))
	}

	for _, s := range segments {
		cfilter.Add(SegmentToBytes(&s))
	}
	return segments
}

func GetRandomSegmentWithId(segmentId int64) qual.Segment {
	return qual.Segment {
		Id:        segmentId,
		Ns:        "mm",
		CreatedAt: time.Now().Unix(),
	}
}
