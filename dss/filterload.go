package dss

import (
	"bytes"
	"time"

	log "github.com/sirupsen/logrus"
	"mediamath.com/device-strategy-service/filters"
	"mediamath.com/device-strategy-service/metrics"
)


type FilterLoaderService struct {
	filterContainer *FilterContainer
	aerospikeConnector    *AerospikeConnector
}

func NewFilterLoaderService(connector *AerospikeConnector,
	filerContainer *FilterContainer) *FilterLoaderService {
	return &FilterLoaderService{filterContainer: filerContainer, aerospikeConnector: connector}
}

func (s *FilterLoaderService) loadFilters(deviceId string) ([]*filters.BloomFilter, bool, error) {
	defer func(begin time.Time) {
		metrics.GetHistogram("QualificationService", "load_filter_total_milliseconds", 20, 20).
			Observe(float64(time.Since(begin).Milliseconds()))
	}(time.Now())

	filter1, filter2, foundInMemory := s.filterContainer.GetFilters(deviceId)
	if foundInMemory {
		// filter is found in memory
		metrics.GetCounter("QualificationService", "found_meter_in_memory").Inc()
		filter1 := filter1.(*filters.BloomFilter)
		filter2 := filter2.(*filters.BloomFilter)
		if filter1.HashFunc == nil || filter2.HashFunc == nil{
			log.Errorf("filter got from map with nil hashFunc")
		}
		return []*filters.BloomFilter{filter1, filter2}, foundInMemory, nil
	}
	metrics.GetCounter("QualificationService", "found_not_meter_in_memory").Inc()
	filtersFromAeroSpike, foundInDb, err := s.loadFilterFromAerospike(deviceId)

	//populate the in memory map
	if foundInDb && err != nil {
		s.filterContainer.SetFilters(deviceId, filtersFromAeroSpike)
	}

	return filtersFromAeroSpike, foundInDb, err
}

func (s *FilterLoaderService) loadFilterFromAerospike(deviceId string) ([]*filters.BloomFilter, bool, error) {
	defer func(begin time.Time) {
		metrics.GetHistogram("QualificationService", "load_filter_from_aerospike_milliseconds", 2, 10).
			Observe(float64(time.Since(begin).Milliseconds()))
	}(time.Now())
	filterBytes, err := s.aerospikeConnector.getFilters(deviceId)
	if err != nil {
		metrics.GetCounter("QualificationService", "error_getting_filter_from_aerospike").Inc()
		return nil, false, err
	}
	if filterBytes == nil {
		metrics.GetCounter("QualificationService", "no_filter_in_aerospike_found").Inc()
		return nil, false, err
	}

	var filtersCreated []*filters.BloomFilter
	for _, bytess := range filterBytes {
		var byteBuffer bytes.Buffer
		byteBuffer.Write(bytess)
		filterCreated, err := filters.DeserializeBloomBytes(byteBuffer)
		if filterCreated.HashFunc == nil {
			log.Errorf("filter got with nil hashFunc")
		}
		if err != nil {
			metrics.GetCounter("QualificationService", "error_getting_decoding_filter").Inc()
			// TODO figure out why this logic fails for bloom filter
			//log.Error("decode error:", err)
			return nil, false, err
		}
		metrics.GetCounter("QualificationService", "fetching_filter_from_aerospike_with_success").Inc()

		filtersCreated = append(filtersCreated, filterCreated)
	}

	return filtersCreated, true, nil
}
