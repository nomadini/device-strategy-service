package dss

import (

	"math"
	"math/rand"
	"strings"
	"time"

	log "github.com/sirupsen/logrus"
	qual "mediamath.com/device-strategy-service/pb"
)

func LoadDeviceSegments(filerContainer *FilterContainer,
	aerospikeConnector *AerospikeConnector,
	loader *FilterLoaderService) {
	cachePersistenceService := NewCachePersistenceService(aerospikeConnector, filerContainer)
	populationService := NewFilterPopulationService(cachePersistenceService, filerContainer, loader)
	numberOfDevicesPopulated := 0
	for {
		minNumberOfSegments := math.Pow(float64(10), float64(rand.Int() % 3 + 2))
		numberOfSegments := (rand.Int() % 100 ) + int(minNumberOfSegments)
		var segments []qual.Segment
		for i := 0; i < numberOfSegments; i++ {
			segments = append(segments, GetRandomSegment())
		}
		segments = append(segments, GetAlwaysSegment())
		deviceId := GetRandomDeviceId()

		if strings.HasSuffix(deviceId, "0") {
			segments = append(segments, GetSegmentForDevice0())
		}

		populationService.Populate(deviceId, segments)
		numberOfDevicesPopulated++
		if numberOfDevicesPopulated % 100 == 1 {
			log.Debugf("populated %d devices, number of segments for this device %d, deviceId : %s",
				numberOfDevicesPopulated, len(segments), deviceId)
		}
		if numberOfDevicesPopulated  >= NUMBER_OF_DEVICES {
			break
		}
	}
}

// all devices will have this segment
func GetAlwaysSegment() qual.Segment {
	return qual.Segment{
		Id:        102,
		Ns:        "mm",
		CreatedAt: time.Now().Unix(),
	}
}

// only devices with ids ending in 0 will have this this segment
func GetSegmentForDevice0() qual.Segment {
	return qual.Segment{
		Id:        103,
		Ns:        "mm",
		CreatedAt: time.Now().Unix(),
	}
}

func GetRandomSegment() qual.Segment {
	return qual.Segment{
		Id:        GetRandomSegmentId(),
		Ns:        "mm",
		CreatedAt: time.Now().Unix(),
	}
}

