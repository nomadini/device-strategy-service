package dss

import (
	"bytes"

	"mediamath.com/device-strategy-service/filters"
	"mediamath.com/device-strategy-service/metrics"
)

type CachePersistenceService struct {
	filterContainer *FilterContainer
	aerospikeConnector *AerospikeConnector
}

func NewCachePersistenceService(aerospikeConnector *AerospikeConnector,
	filterContainer *FilterContainer) *CachePersistenceService {
	return &CachePersistenceService{filterContainer: filterContainer,
		aerospikeConnector: aerospikeConnector}
}

//TODO write a good test that confirms persistence works
func (s *CachePersistenceService) PersistFilter(deviceId string, bloomFilters [] *filters.BloomFilter) {
	var buffers [] bytes.Buffer
	for i := range bloomFilters {
		buffer, err := filters.SerializeBloomFilter(bloomFilters[i])
		if err != nil {
			metrics.GetCounter("CachePersistenceService", "error_serializing_filter").Inc()
			return
		}
		buffers = append(buffers, buffer)
	}

	err := s.aerospikeConnector.persistDeviceIdFilterBytes(deviceId, buffers)
	//TODO send these bytes to kafka topic for all other pods to receive it and persist it in their KV and memory caches as well
	if err != nil {
		metrics.GetCounter("CachePersistenceService","error_setting_key_in_aerospike").Inc()
		//log.Errorf("error setting key %v", err)
		return
	}

	metrics.GetCounter("CachePersistenceService", "persisted_filter_in_aerospike").Inc()
}


