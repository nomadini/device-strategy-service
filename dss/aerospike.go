package dss

import (
	"bytes"
	"fmt"
	"time"

	"github.com/aerospike/aerospike-client-go"
	"github.com/aerospike/aerospike-client-go/types"
	log "github.com/sirupsen/logrus"
	"mediamath.com/device-strategy-service/metrics"
)

// AerospikeConnector ...
type AerospikeConnector struct {
	ConnType     string
	Seeds        []string
	namespace    string
	set          string
	ttlInSeconds int
	Client       *aerospike.Client
	readPolicy   *aerospike.BasePolicy
}

// NewAerospikeConnector ...
func NewAerospikeConnector(host string, port int, namespace string,
	set string, ttlInSeconds int) *AerospikeConnector {
	client, err := aerospike.NewClient(host, port)
	if err != nil {
		log.Fatal("cannot connect to aerospike")
	}

	readPolicy := aerospike.BasePolicy{
		ConsistencyLevel: aerospike.CONSISTENCY_ONE,
		TotalTimeout:     time.Millisecond * 100,
		MaxRetries:       2,
	}

	connector := AerospikeConnector{
		Client:       client,
		namespace:    namespace,
		set:          set,
		ttlInSeconds: ttlInSeconds,
		readPolicy:   &readPolicy,
	}

	return &connector
}

// Close ...
func (a *AerospikeConnector) Close() error {
	a.Client.Close()
	return nil
}

// getFilters ...
func (a *AerospikeConnector) getFilters(inputKey string) ([][]byte, error) {

	key, err := aerospike.NewKey(a.namespace, a.set, inputKey)
	//log.Debug("reading key : ", inputKey)
	if err != nil {
		log.Error("error creating key")
		return nil, err
	}

	var retVal []byte

	record, err := a.Client.Get(a.readPolicy, key)

	if aerr, ok := err.(types.AerospikeError); ok {
		resultCode := aerr.ResultCode()
		switch {
		case record == nil:
			metrics.GetCounter("AerospikeConnector", "record_is_nill").Inc()
			return nil, nil
		case resultCode == types.KEY_NOT_FOUND_ERROR:
			log.Trace("no match found for key ", inputKey)
			metrics.GetCounter("AerospikeConnector", "no_match_found_for_key").Inc()
			return nil, nil
		default:
			log.Trace("found match for key", inputKey)
			metrics.GetCounter("AerospikeConnector", "match_found_for_key").Inc()
			retVal = []byte(fmt.Sprintf("%v", record.Bins["data"]))
			retVal2 := []byte(fmt.Sprintf("%v", record.Bins["data2"]))
			return [][]byte{retVal, retVal2}, nil
		}

	}

	return nil, err
}

// persistDeviceIdFilterBytes ...
func (a *AerospikeConnector) persistDeviceIdFilterBytes(inputKey string,
	buffers [] bytes.Buffer) error {
	//timerStart := time.Now()
	key, err := aerospike.NewKey(a.namespace, a.set, inputKey)
	if err != nil {
		log.Error("error creating key")
		return err
	}

	policy := aerospike.NewWritePolicy(0, uint32(a.ttlInSeconds)) //10 hours
	policy.MaxRetries = 10
	bins := aerospike.BinMap{
		"data": string(buffers[0].Bytes()),
		"data2": string(buffers[1].Bytes()),
		"key":  inputKey,
	}

	err = a.Client.Put(policy, key, bins)

	return err
}
