package dss

import (
	"hash/crc64"

	"github.com/dgraph-io/ristretto"
	log "github.com/sirupsen/logrus"
	"mediamath.com/device-strategy-service/filters"
	"mediamath.com/device-strategy-service/metrics"
)

type FilterContainer struct {
	cache1 *ristretto.Cache
	cache2 *ristretto.Cache
}

func (c FilterContainer) initFilters(deviceId string, sizeOfFilter uint) [] *filters.BloomFilter {
	filterA := filters.NewBloomFilter(sizeOfFilter, 0.001)
	filterB := filters.NewBloomFilter(sizeOfFilter, 0.001)
	filterB.SetHash(crc64.New(crc64.MakeTable(crc64.ECMA)))

	c.SetFilters(deviceId, []*filters.BloomFilter{filterA, filterB})
	return []*filters.BloomFilter{filterA, filterB}
}

func (c FilterContainer) addBytesToFilters(bytess []byte, filters[] *filters.BloomFilter) {
	for i := range filters {
		if filters[i].Count() <= filters[i].Capacity() {
			_ = filters[i].Add(bytess)
		} else {
			log.Error("filter is exceeding the capacity %d:%d",
				filters[i].Count(), filters[i].Capacity())
		}
	}
}

func (c FilterContainer) GetFilters(id string) (interface{}, interface{}, bool) {
	var val interface{}
	var val2 interface{}
	var ok bool
	var ok2 bool
	if val, ok = c.cache1.Get(id); val == nil || !ok {
		return nil, nil, ok
	}
	if val2, ok2 = c.cache2.Get(id); val2 == nil || !ok2 {
		return nil, nil, ok2
	}

	return val, val2, ok && ok2
}

func (c FilterContainer) SetFilters(deviceId string, filters []*filters.BloomFilter) {
	if c.cache1.Set(deviceId, filters[0], 1) {
		metrics.GetCounter("FilterContainer", "cache1_size").Inc()
	}

	if c.cache2.Set(deviceId, filters[1], 1) {
		metrics.GetCounter("FilterContainer", "cache2_size").Inc()
	}

}

func NewFilterContainer(cache1Arg *ristretto.Cache, cache2Arg *ristretto.Cache) *FilterContainer {
	return &FilterContainer{cache1: cache1Arg, cache2: cache2Arg}
}

