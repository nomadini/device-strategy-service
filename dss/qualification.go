package dss

import (
	"context"
	"errors"
	"math"
	"strconv"
	"sync"

	//"sync"
	"time"

	log "github.com/sirupsen/logrus"
	"mediamath.com/device-strategy-service/filters"
	"mediamath.com/device-strategy-service/metrics"
	"mediamath.com/device-strategy-service/pb"
)

type QualificationService struct {
	filterContainer *FilterContainer
	loader          *FilterLoaderService
}

func NewQualificationService(loader *FilterLoaderService, filerContainer *FilterContainer) *QualificationService {
	return &QualificationService{loader: loader, filterContainer: filerContainer}
}

// finds the qualified strategies for a device id
func (s *QualificationService) GetQualifiedStrategies(ctx context.Context,
	req *qual.QualificationRequest) (*qual.QualificationReply, error) {
	qualified, err := s.qualify(req.DeviceId, req.Strategies)
	reply := qual.QualificationReply{Qualified: qualified}

	return &reply, err
}

func (s *QualificationService) qualify(deviceId string, inputStrategies []*qual.Strategy) ([]*qual.Strategy, error) {
	defer func(begin time.Time) {
		categoryDivider := 100
		if len(inputStrategies) >= 1000 {
			categoryDivider = 1000
		}
		sizeRounded := int64(math.Trunc(float64(len(inputStrategies) / categoryDivider)))
		log.Tracef("len(inputStrategies) %d , sizeRounded : %d \n", len(inputStrategies), sizeRounded)
		sizeRounded = int64(categoryDivider) * sizeRounded
		sizeOfStrategies := strconv.FormatInt(sizeRounded, 10)
		metrics.GetHistogram("QualificationService", "qualify_size_"+sizeOfStrategies+"_milliseconds", 20, 20).
			Observe(float64(time.Since(begin).Milliseconds()))
	}(time.Now())

	//log.Debug("qualify for deviceId : ", deviceId)
	bloomFilters, foundFilters, err := s.loader.loadFilters(deviceId)
	if !foundFilters {
		metrics.GetCounter("QualificationService", "warning_filter_not_found_for_deviceId").Inc()
		return nil, errors.New("filter_not_found_for_device_id")
	}

	if err != nil {
		metrics.GetCounter("QualificationService", "empty_result_because_error").Inc()
		return nil, err
	}

	return s.checkAllStrategies(inputStrategies, bloomFilters), nil
}

func (s *QualificationService) checkAllStrategies(inputStrategies []*qual.Strategy,
	filters []*filters.BloomFilter) []*qual.Strategy {
	defer func(begin time.Time) {
		categoryDivider := 100
		if len(inputStrategies) >= 1000 {
			categoryDivider = 1000
		}
		sizeRounded := int64(math.Trunc(float64(len(inputStrategies) / categoryDivider)))
		log.Tracef("len(inputStrategies) %d , sizeRounded : %d \n", len(inputStrategies), sizeRounded)
		sizeRounded = int64(categoryDivider) * sizeRounded
		sizeOfStrategies := strconv.FormatInt(sizeRounded, 10)
		metrics.GetHistogram("QualificationService", "check_all_strategies_"+sizeOfStrategies+"_milliseconds", 5, 20).
			Observe(float64(time.Since(begin).Milliseconds()))
	}(time.Now())

	allResponseStrategies := make([]*qual.Strategy, len(inputStrategies))
	var wg sync.WaitGroup
	for index, _ := range inputStrategies {
		wg.Add(1)
		go func(indexArg int) {
			strg := s.checkOneStrategy(inputStrategies[index], filters)
			strg.Id = inputStrategies[index].Id
			allResponseStrategies[indexArg] = strg
			wg.Done()
		}(index)
	}
	wg.Wait()

	// SEQUENTIAL
	//allResponseStrategies := make([]*qual.Strategy, 0, len(inputStrategies))
	//for index, _ := range inputStrategies {
	//	inputStrategy := inputStrategies[index]
	//
	//	strg := s.checkOneStrategy(inputStrategy, filters)
	//	strg.Id = inputStrategy.Id
	//	//allResponseStrategiesChannel <- strg
	//	allResponseStrategies = append(allResponseStrategies, strg)
	//}

	return allResponseStrategies
}

func (s *QualificationService) checkOneStrategy(strategy *qual.Strategy, filters []*filters.BloomFilter) *qual.Strategy {
	response := &qual.Strategy{}

	defer func(begin time.Time) {
		metrics.GetHistogram("QualificationService", "check_one_strategy_microseconds", 200, 10).
			Observe(float64(time.Since(begin).Microseconds()))
	}(time.Now())
	for sIndex, _ := range strategy.Segments {
		segment := strategy.Segments[sIndex]

		buf := SegmentToBytes(segment)
		result := s.checkFilter(filters[0], buf)
		if result {
			result2 := s.checkFilter(filters[1], buf)
			if result && result2 {
				metrics.GetCounter("QualificationService", "segment_matched_for_device").Inc()
				log.Trace("segment matched for device")
				response.Segments = append(response.Segments, segment)
			}
		} else {
			log.Trace("segment not matched for device")
			metrics.GetCounter("QualificationService", "segment_NOT_matched_for_device").Inc()
		}
	}
	return response
}

func (s *QualificationService) checkFilter(filter *filters.BloomFilter, buf []byte) bool {
	defer func(begin time.Time) {
		metrics.GetHistogram("QualificationService", "check_filter_microsecond", 1, 10).
			Observe(float64(time.Since(begin).Microseconds()))
	}(time.Now())
	//TODO later add a cache that holds this result for frequently seen deviceIds
	result := filter.Test(buf)
	if result {
		metrics.GetCounter("QualificationService", "one_filter_found_match").Inc()
	}
	return result
}
