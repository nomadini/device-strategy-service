package dss

import (
	"mediamath.com/device-strategy-service/metrics"

	qualification "mediamath.com/device-strategy-service/pb"
)


type FilterPopulationService struct {
	filterContainer *FilterContainer
	loader *FilterLoaderService
	cachePersistenceService *CachePersistenceService
}

func NewFilterPopulationService(cachePersistenceService *CachePersistenceService,
	filterContainer *FilterContainer,
	loader *FilterLoaderService) *FilterPopulationService {
	return &FilterPopulationService{filterContainer: filterContainer,
		cachePersistenceService: cachePersistenceService,
		loader: loader}
}

//TODO add kafka consumer that calls populate method and http and grpc methods
func (s *FilterPopulationService) Populate(deviceId string, segments []qualification.Segment) {
	_, ok, _ := s.loader.loadFilters(deviceId)
	if !ok {

		filters := s.filterContainer.initFilters(deviceId, uint(len(segments)))
		//adding segments to the filter in the map
		for index := range segments {
			//TODO add namespace too.
			bytes := SegmentToBytes(&segments[index])
			s.filterContainer.addBytesToFilters(bytes, filters)
		}
		metrics.GetCounter("FilterPopulationService", "populating_new_device_in_filter_map").Inc()
		//persisting the filter
		s.cachePersistenceService.PersistFilter(deviceId, filters)
	} else {
		metrics.GetCounter("FilterPopulationService", "device_is_already_populated").Inc()
	}
}
