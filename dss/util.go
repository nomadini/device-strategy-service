package dss

import (
	"encoding/binary"
	"hash/fnv"
	"math/rand"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
	"mediamath.com/device-strategy-service/metrics"
	qual "mediamath.com/device-strategy-service/pb"
)

var NUMBER_OF_DEVICES = 100

func GetRandomSegmentId() int64 {
	return int64(rand.Int() % 20000 + 10000000);
}

func GetRandomDeviceId() string {
	//deviceId := random.String(12)
	deviceId := "device" + strconv.FormatInt(int64(rand.Int() % NUMBER_OF_DEVICES), 10)
	return deviceId
}

func hashString(s string) int64 {
	h := fnv.New64()
	_, err := h.Write([]byte(s))
	if err != nil {
		log.Panic("error in writing bytes", err)
	}
	return int64(h.Sum64())
}

func SegmentToBytes(segment *qual.Segment) []byte {
	defer func(begin time.Time) {
		metrics.GetHistogram("QualificationService", "segment_to_bytes_microseconds", 1, 10).
			Observe(float64(time.Since(begin).Microseconds()))
	}(time.Now())

	nsHash := hashString(segment.Ns)
	buf := make([]byte, binary.MaxVarintLen64)
	binary.PutVarint(buf, segment.Id + nsHash)
	return buf
}

