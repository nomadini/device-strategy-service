package main

import (
	"context"
	"flag"
	"math/rand"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/status"
	"mediamath.com/device-strategy-service/dss"
	"mediamath.com/device-strategy-service/metrics"
	qual "mediamath.com/device-strategy-service/pb"
)

var (
	serverAddr = flag.String("server_addr", "localhost:10000", "The server address in the format of host:port")
)

// getQualifiedSegments receives a sequence of route notes, while sending notes for various locations.
func getQualifiedSegments(client qual.QualifyClient) {

	deviceId := dss.GetRandomDeviceId();
	srtgs := []*qual.Strategy{
		{Id: getRandomStrategyId(), Segments: getSegments()},
	}
	for i:= 0; i < getRandomNumberOfCandidateStrategiesPerDevice(); i++ {

		strategy := qual.Strategy{Id: getRandomStrategyId(), Segments: getSegments()}
		srtgs = append(srtgs, &strategy)
	}


	reply, err := callServer(deviceId, srtgs, client)
	//log.Debugf("deviceId : %s reply : %v, err %v", deviceId, reply, err)
	if err != nil {
		return
	}
	if false {
		for index := range reply.Qualified {
			q := reply.Qualified[index]
			log.Debugf("qualified strategy id : %d", q.Id)
			log.Debugf("qualified number of qualified segments : %d", len(q.Segments))
			for i, _ := range q.Segments {
				log.Debugf("qualified segment id : %d", q.Segments[i].Id)
			}
		}
	}
}

func callServer(deviceId string, srtgs []*qual.Strategy, client qual.QualifyClient)(*qual.QualificationReply, error) {
	in := &qual.QualificationRequest{DeviceId: deviceId, Strategies: srtgs}
	ctx, cancel := context.WithTimeout(context.Background(), 100 * time.Millisecond)
	defer cancel()
	reply, err := client.GetQualifiedStrategies(ctx, in)
	defer func(begin time.Time, err error) {
		name := "call_server_microseconds_"
		if err != nil {
			name += "_with_error"
		}
		metrics.GetHistogram("Client", name, 100, 10).
			Observe(float64(time.Since(begin).Microseconds()))
	}(time.Now(), err)
	if err != nil {
		//log.Tracef("error from server %v", err)
		stat, _ := status.FromError(err)
		msg := strings.ReplaceAll(stat.Message(), " ", "_")
		msg = strings.ReplaceAll(msg, "\"", "_")
		msg = strings.ReplaceAll(msg, ":", "_")
		msg = strings.ReplaceAll(msg, "=", "_")
		msg = strings.ReplaceAll(msg, "[", "")
		msg = strings.ReplaceAll(msg, "]", "")
		metrics.GetCounter("Client", "error_from_server_" + msg).Inc()
	}
	return reply, err
}

func getSegments() []*qual.Segment {
	var segments []*qual.Segment
	for i:= 0; i < getRandomNumberOfSegmentsPerStrategy(); i++ {
		seg := qual.Segment{
			Id: dss.GetRandomSegmentId(),
			Ns: "mm",
			CreatedAt: 1581807520,
		}
		segments = append(segments, &seg)
		alwaysSegment := dss.GetAlwaysSegment()
		segments = append(segments, &alwaysSegment)
	}
	return segments
}

func getRandomStrategyId() int64 {
	return int64(rand.Int() % 200000 + 10000000);
}

func getRandomNumberOfSegmentsPerStrategy() int {
	return rand.Int() % 20 + 400;
}

func getRandomNumberOfCandidateStrategiesPerDevice() int {
	return rand.Int() % 20 + 100;
}

func main() {
	logrus.SetLevel(logrus.DebugLevel)

	flag.Parse()
	var opts []grpc.DialOption
	opts = append(opts, grpc.WithInsecure())
	opts = append(opts, grpc.WithBlock())
	conn, err := grpc.Dial(*serverAddr, opts...)
	if err != nil {
		log.Fatalf("fail to dial: %v", err)
	}
	defer conn.Close()
	client := qual.NewQualifyClient(conn)
	go func() {
		// metrics available at http://localhost:2112/metrics
		http.Handle("/metrics", promhttp.Handler())
		err := http.ListenAndServe(":2114", nil)

		if err != nil {
			panic(err)
		}
	}()

	var wg sync.WaitGroup
	for i := 0; i < 2; i++ {
		wg.Add(1)
		go func() {
			for {
				getQualifiedSegments(client)
				//time.Sleep(100 * time.Millisecond)
			}
			wg.Done()
		}()
	}
	wg.Wait()
}