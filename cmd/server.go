package main

import (
	"flag"
	"fmt"
	"net"
	"net/http"
	_ "net/http/pprof"
	"os"
	"os/signal"
	"runtime"
	"runtime/pprof"
	"syscall"

	"github.com/dgraph-io/ristretto"
	"github.com/dgraph-io/ristretto/z"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"mediamath.com/device-strategy-service/dss"
	qual "mediamath.com/device-strategy-service/pb"
)

var (
	port = flag.Int("port", 10000, "The server port")
)

func main() {
	handleSignals()
	// Setting up cpu profiler
	var cpuprofile = flag.String("cpuprofile", "/tmp/cpuprofile", "write cpu profile to `file`")
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal("could not create CPU profile: ", err)
		}
		defer f.Close()
		if err := pprof.StartCPUProfile(f); err != nil {
			log.Fatal("could not start CPU profile: ", err)
		}
		defer pprof.StopCPUProfile()
	}

	logrus.SetLevel(logrus.DebugLevel)
	flag.Parse()
	lis, err := net.Listen("tcp", fmt.Sprintf("localhost:%d", *port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	var opts []grpc.ServerOption

	grpcServer := grpc.NewServer(opts...)
	connector := dss.NewAerospikeConnector("localhost", 3000, "test", "dss", 360000) // 100 hours
	//cuckooMap := hashmap.New(2 ^ 20) // 1 million entries
	lruCache1, err := ristretto.NewCache(createConfig())
	lruCache2, err2 := ristretto.NewCache(createConfig())
	if err != nil || err2 != nil {
		panic(err)
	}
	filerContainer := dss.NewFilterContainer(lruCache1, lruCache2)
	loader := dss.NewFilterLoaderService(connector, filerContainer)
	go func() {
		dss.LoadDeviceSegments(filerContainer, connector, loader)
	}()

	go func() {
		// metrics available at http://localhost:2112/metrics
		http.Handle("/metrics", promhttp.Handler())
		err := http.ListenAndServe(":2112", nil)

		if err != nil {
			panic(err)
		}
	}()

	qual.RegisterQualifyServer(grpcServer, dss.NewQualificationService(loader, filerContainer))
	err = grpcServer.Serve(lis)
	if err != nil {
		panic(err)
	}
}

func createConfig() *ristretto.Config {
	return &ristretto.Config{
		NumCounters: 1e6,
		MaxCost:     1e5, // 100k
		Cost:        func(value interface{}) (int64) {
			return int64(1)
		},
		BufferItems: 64,
		Metrics:     true,
		KeyToHash: func(key interface{}) (uint64, uint64) {
			return z.KeyToHash(key)
		},
	}

}

func profileMemory() {
	var memprofile = flag.String("memprofile", "/tmp/memprofile", "write memory profile to `file`")
	if *memprofile != "" {
		f, err := os.Create(*memprofile)
		if err != nil {
			log.Fatal("could not create memory profile: ", err)
		}
		defer f.Close()
		runtime.GC() // get up-to-date statistics
		if err := pprof.WriteHeapProfile(f); err != nil {
			log.Fatal("could not write memory profile: ", err)
		}
	}
}

// HandleSignals ...
func handleSignals() {
	log.Debug("Setting up signal handler ...")
	signalChan := make(chan os.Signal, 1)

	signal.Notify(signalChan,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT)

	go func() {
		s := <-signalChan
		log.Debugf("Signal: %#v", s)
		switch s {
		case syscall.SIGINT: // ^C
			log.Debug("Received SIGNINT signal; quitting ...")
			profileMemory()
			os.Exit(0)
		case syscall.SIGTERM:
			log.Debug("Received SIGTERM signal; quitting ...")
			profileMemory()
			os.Exit(0)
		case syscall.SIGQUIT:
			log.Debug("Received SIGQUIT signal; quitting ...")
			profileMemory()
			os.Exit(1)
		default:
			log.Debugf("Received unexpected signal %#v", s)
			profileMemory()
		}
	}()
	log.Info("Signal handler is set up.")
}
