## What is device-strategy-service (DSS)?
It's a service that uses bloom filters to determine if a device has a specific strategy or not.

### DSS advantages
DSS has a few advantages compared to the current scheme.
1. it saves the burden of managing a 40T databases of segments data in the form of
   three layer cache system that we already have in place, using hundreds of machines.
2. it saves the network transfer cost/time by eliminating the need to bring in the segment
   data for every single request.
3. it will reduce the CPU usage on every bidder by eliminating the need for iterating through
   every segment to determine if they are good for strategies
4. it will reduce the memory usage on every bidder by eliminating the need for having a local
   cache of Segment Data on the host, which currently is about 6G.

#### Request and Response example

``` 
request body
{ 
    "device_id" : "CCF6DC12B98AEB2346AFE1BEE7860DF01FDE158B"
    // this is an optional set, it doesn't have to be present
    "strategy_set": [12, 34, 45, 56, 67] // ids of each strategy in t1db
}

 response body 
{
    "qualified": [56, 67]
}
```


### Dashboard and Metrics
The server metrics are located at http://localhost:2112/metrics
The client metrics are located at http://localhost:2114/metrics

### How to run it locally?
    1. start the aerospike instance in docker/docker-compose.yml 
    2. run the server.go and then client.go l
    3. check the  http://localhost:2114/metrics and  http://localhost:2112/metrics for client and server side metrics
    
### How to profile?
    To debug/profile the app 
        go tool pprof http://localhost:2112/debug/pprof/heap
        and run top5
        or run top5 -cum
	    go tool pprof http://localhost:2112/debug/pprof/profile?seconds=30
	
	
