appName=$1
appHost=$2
appPort=$3
tempFilePath=/tmp/${appName}_check_up.txt
echo "Waiting ${appName} to launch on ${appPort}..."

check() {
  grep -q 'connected' ${tempFilePath}
  result=$?
    if [ $result -eq 0 ] ; then
      echo "confirmed ${appName} is up....."
      exit 0
    fi
  wget --timeout=1 --tries=1 ${appHost}:${appPort} > ${tempFilePath} 2>&1
}

check

rm -f ${tempFilePath}

while true; do
  check
  cat ${tempFilePath} #for debugging purposes
  sleep 3 # wait for 1 second before check again
  echo "checking again..."
done

echo "${appName} launched"