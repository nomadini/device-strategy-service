package metrics

import (
	"sync"

	"github.com/cornelk/hashmap"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var (
	counterMap   = hashmap.New(2 ^ 7) // 255
	histogramMap = hashmap.New(2 ^ 8) // 255
	summaryMap   = hashmap.New(2 ^ 8) // 255
	meterMutex   sync.Mutex
)

func GetCounter(module, name string) prometheus.Counter {
	counter, ok := counterMap.GetStringKey(name)
	if !ok {
		meterMutex.Lock()
		defer meterMutex.Unlock()

		//after acquiring the lock, check the map again
		counter, ok = counterMap.GetStringKey(name)
		if !ok {
			counter = promauto.NewCounter(prometheus.CounterOpts{
				Namespace: "mediamath_dss",
				Subsystem: module,
				Name: name,
			})

			counterMap.Set(name, counter)
			return counter.(prometheus.Counter)
		}
	}

	return counter.(prometheus.Counter)
}

func GetHistogram(module, name string, bucketWidth float64, numberOfBuckets int) prometheus.Histogram {
	meter, ok := histogramMap.GetStringKey(name)
	if !ok {
		meterMutex.Lock()
		defer meterMutex.Unlock()

		//after acquiring the lock, check the map again
		meter, ok = histogramMap.GetStringKey(name)
		if !ok {
			meter = promauto.NewHistogram(prometheus.HistogramOpts{
				Name:      name,
				Namespace: "mediamath_dss",
				Subsystem: module,
				Buckets:   prometheus.LinearBuckets(0, bucketWidth, numberOfBuckets),
			})
			histogramMap.Set(name, meter)
			return meter.(prometheus.Histogram)
		}
	}

	return meter.(prometheus.Histogram)
}

func GetSummary(module, name string, bucketWidth float64, numberOfBuckets int) prometheus.Summary {
	meter, ok := summaryMap.GetStringKey(name)
	if !ok {
		meterMutex.Lock()
		defer meterMutex.Unlock()

		//after acquiring the lock, check the map again
		meter, ok = summaryMap.GetStringKey(name)
		if !ok {
			meter = promauto.NewSummary(prometheus.SummaryOpts{
				Name:       name,
				Namespace:  "mediamath_dss",
				Subsystem:  module,
				Objectives: map[float64]float64{0.5: 0.05, 0.9: 0.01, 0.99: 0.001},
			})
			summaryMap.Set(name, meter)
			return meter.(prometheus.Summary)
		}
	}
	return meter.(prometheus.Summary)

}